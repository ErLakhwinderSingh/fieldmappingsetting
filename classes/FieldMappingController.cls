public with sharing class FieldMappingController  {

    
    @AuraEnabled
    public static string insertFieldMappingSettingAndChildMdt(
        string parentDeveloperName,
        list<Field_Mapping_Setting__mdt> fieldMappingStgRecordList,
        list<Field_Mapping_Setting_Child__mdt> fieldMappingSettingChildList,
        list<string> deleteFieldMappingChildRecord 
        ){
        try{
            Field_Mapping_Setting__mdt getInstFieldMappingStgMdt1;
            string fieldMappingStgId, fieldMapStgchildId;
                if(fieldMappingStgRecordList != null){
                    for(Field_Mapping_Setting__mdt fieldMapStgobj:fieldMappingStgRecordList)
                    {
                        fieldMappingStgId = insertFieldMappingSettingMdt(
                                                                        fieldMapStgobj.DeveloperName, 
                                                                        fieldMapStgobj.Object_Api_Name__c,
                                                                        fieldMapStgobj.Object_Label__c,
                                                                        fieldMapStgobj.Action_Name__c,
                                                                        fieldMapStgobj.Action_Type__c
                                                                        );
                        
                            system.debug('fieldMappingStgId'+fieldMappingStgId);
                        //     system.debug('allready exist object '+ fieldMapStgobj.MasterLabel+ 'Id ='+ fieldMapStgobj.Id);
                    }
                }
                getInstFieldMappingStgMdt1 = Field_Mapping_Setting__mdt.getInstance(parentDeveloperName);
                system.debug('getInstFieldMappingStgMdt1  =' +getInstFieldMappingStgMdt1);
                if(getInstFieldMappingStgMdt1 != null)
                {
                    for(Field_Mapping_Setting_Child__mdt fieldMapStgChildobj: fieldMappingSettingChildList)
                    {
                       
                        system.debug('sobject= '+parentDeveloperName);
                            fieldMapStgchildId= insertFieldMappingSettingChildMdt(
                                                                    fieldMapStgChildobj.DeveloperName,
                                                                    fieldMapStgChildobj.Field_Label__c,
                                                                    fieldMapStgChildobj.Field_Api_Name__c,
                                                                    getInstFieldMappingStgMdt1.QualifiedApiName,
                                                                    fieldMapStgChildobj.Field_Type__c,
                                                                    fieldMapStgChildobj.Field_Value__c,
                                                                    fieldMapStgChildobj.Is_Deleted1__c
                                                                    );
                            system.debug('fieldMapStgchildId'+fieldMapStgchildId);
                    }
                }
                if(deleteFieldMappingChildRecord != null)
                {
                    deleteMetadataChildRecord(deleteFieldMappingChildRecord);
                }
                
               

            system.debug('R=='+getInstFieldMappingStgMdt1);
            if(getInstFieldMappingStgMdt1 != null )
            {
                return 'Field Mapping Stg Exist';
            }
            
            else
            {
                return null;
            }
        //return null;
        }catch (Exception e) {
            system.debug('error '+e);
            throw new AuraHandledException(e.getMessage()); 
        }

    }
    @future(callout = true)
    public static void deleteMetadataChildRecord(list<String> delList) {
        try{
            for(string devName: delList){
                string deleteRecord = 'Field_Mapping_Setting_Child__mdt.'+devName;
                MetadataService.MetadataPort service = createService();
                List<String> recordsToDelete = new List<String>();
                recordsToDelete.add(deleteRecord);
                service.deleteMetadata('CustomMetadata', recordsToDelete);
            }

        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    public static MetadataService.MetadataPort createService() {
        try{
            MetadataService.MetadataPort service = new MetadataService.MetadataPort();
            service.SessionHeader = new MetadataService.SessionHeader_element();
            service.SessionHeader.sessionId = UserInfo.getSessionId();
            return service;
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        
    }
   
    public static Id insertFieldMappingSettingMdt(
        string devName,
        string sObjectApiName,
        string sObjectLabel,
        string actionName,
        string actionType
        ){
        try{
            Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
            String nameSpacePrefix ='';
            
            Metadata.CustomMetadata firstMetadataRec =  new Metadata.CustomMetadata();
            firstMetadataRec.fullName = nameSpacePrefix + 'Field_Mapping_Setting__mdt.'+devName;
            firstMetadataRec.label = devName; 
            
            Metadata.CustomMetadataValue customField1 = new Metadata.CustomMetadataValue();
            customField1.field = 'Object_Api_Name__c';
            customField1.value = sObjectApiName;
            firstMetadataRec.values.add(customField1);

            Metadata.CustomMetadataValue customField2 = new Metadata.CustomMetadataValue();
            customField2.field = 'Object_Label__c';
            customField2.value = sObjectLabel;
            firstMetadataRec.values.add(customField2);

            Metadata.CustomMetadataValue customField3 = new Metadata.CustomMetadataValue();
            customField3.field = 'Action_Name__c';
            customField3.value = actionName;
            firstMetadataRec.values.add(customField3);

            Metadata.CustomMetadataValue customField4 = new Metadata.CustomMetadataValue();
            customField4.field = 'Action_Type__c';
            customField4.value = actionType;
            firstMetadataRec.values.add(customField4);
            
            mdContainer.addMetadata(firstMetadataRec);
                           
             
            Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, null);
            return jobId;

        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
     public static Id insertFieldMappingSettingChildMdt(
        string developerName,
        string fieldLabel,
        string fieldApiName,
        string fieldMappingSetting,
        string fieldType,
        string fieldValue,
        Boolean IsDeleted
                ){
                
        try{
            Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
            String nameSpacePrefix ='';
                
            Metadata.CustomMetadata firstMetadataRec =  new Metadata.CustomMetadata();
            firstMetadataRec.fullName = nameSpacePrefix + 'Field_Mapping_Setting_Child__mdt.'+developerName;
            firstMetadataRec.label = developerName; 

            Metadata.CustomMetadataValue customField1 = new Metadata.CustomMetadataValue();
            customField1.field = 'Field_Api_Name__c';
            customField1.value = fieldApiName;
            firstMetadataRec.values.add(customField1);

            Metadata.CustomMetadataValue customField2 = new Metadata.CustomMetadataValue();
            customField2.field = 'Field_Label__c';
            customField2.value = fieldLabel;
            firstMetadataRec.values.add(customField2);

            Metadata.CustomMetadataValue customField3 = new Metadata.CustomMetadataValue();
            customField3.field = 'Field_Mapping_Setting__c';
            customField3.value = fieldMappingSetting;
            firstMetadataRec.values.add(customField3);

            Metadata.CustomMetadataValue customField4 = new Metadata.CustomMetadataValue();
            customField4.field = 'Field_Type__c';
            customField4.value = fieldType;
            firstMetadataRec.values.add(customField4);

            Metadata.CustomMetadataValue customField5 = new Metadata.CustomMetadataValue();
            customField5.field = 'Field_Value__c';
            customField5.value = fieldValue;
            firstMetadataRec.values.add(customField5);

            Metadata.CustomMetadataValue customField6 = new Metadata.CustomMetadataValue();
            customField6.field = 'Is_Deleted1__c';
            customField6.value = IsDeleted;
            firstMetadataRec.values.add(customField6);

             mdContainer.addMetadata(firstMetadataRec);
             
                
            Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, null);
            
            system.debug('jobId  ='+jobId);
        return jobId;

        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static  list<SobjectFieldsListWrapper> getFieldList(String sObjectName){
        try {
            list<SobjectFieldsListWrapper> sObjFieldList = new list<SobjectFieldsListWrapper>();
            if (String.isNotBlank(sobjectName)) {
     
                Map<String, Schema.SObjectField> fieldsMap = Schema.getGlobalDescribe()
                  .get(sobjectName)
                  .getDescribe()
                  .fields.getMap();
                  system.debug('List of fields= '+fieldsMap);
                  List<String> requiredFieldList = new List<String>();
                  for (String sobjField : fieldsMap.keySet()) {
                      Schema.DescribeFieldResult fieldResult = fieldsMap.get(sobjField).getDescribe();
                      SobjectFieldsListWrapper sObjFieldsWrapObj = new SobjectFieldsListWrapper();
                      if (fieldResult.isCreateable()  && !fieldResult.isNillable() && !fieldResult.isDefaultedOnCreate()) 
                      {
                        
                        sObjFieldsWrapObj.label = fieldResult.getLabel();
                        sObjFieldsWrapObj.value = fieldResult.getName();
                        sObjFieldsWrapObj.type = string.valueOf(fieldResult.getType());
                        sObjFieldsWrapObj.isReqField = true;
                        sObjFieldsWrapObj.fieldValue = '';
                        sObjFieldsWrapObj.Id = '';
                        //reqFieldsWrapObj.type = ieldResult.getType();
                        sObjFieldList.add(sObjFieldsWrapObj);
                        //System.debug(sobjField);
                    }  
                    else
                    {
                        sObjFieldsWrapObj.label = fieldResult.getLabel();
                        sObjFieldsWrapObj.value = fieldResult.getName();
                        sObjFieldsWrapObj.type = string.valueOf(fieldResult.getType());
                        sObjFieldsWrapObj.isReqField = false;
                        sObjFieldsWrapObj.Id = '';
                        sObjFieldList.add(sObjFieldsWrapObj);
                    }
                }   
                  
              }
              system.debug(sObjFieldList);
           return sObjFieldList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    @AuraEnabled
    public static List<SobjectListWrapper> getPickListValuesIntoList(string sObjectApiName, string fieldApiName ){
        try{
            List<SobjectListWrapper> pickListValuesList= new List<SobjectListWrapper>();
        Schema.DescribeFieldResult fieldResult = Schema.getGlobalDescribe()
        .get(sObjectApiName)
        .getDescribe()
        .fields.getMap()
        .get(fieldApiName)
        .getDescribe();
        system.debug('List of fields= '+fieldResult);

         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            SobjectListWrapper wrapObj = new SobjectListWrapper();
            wrapObj.value=pickListVal.getLabel();
            wrapObj.label=pickListVal.getLabel();

                pickListValuesList.add(wrapObj);
        }     
        return pickListValuesList;
        }
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }  


    @AuraEnabled
    public static FieldsListMainWrapper getFieldMappingQueryLists(){
        FieldsListMainWrapper mainWrapperList = new FieldsListMainWrapper();
        
        List<Field_Mapping_Setting_Child__mdt> fieldMappingStgChildObj = getFieldMappingSettingChildMdt();
        List<Field_Mapping_Setting__mdt> fieldMappingStgObj = getFieldMappingSettingMdt();
        list<SobjectListWrapper> sObjectobj= getObjectList();
        mainWrapperList.fieldMappingStgChildList = fieldMappingStgChildObj;
        mainWrapperList.fieldMappingStgList = fieldMappingStgObj;
        mainWrapperList.sObjectList= sObjectobj;
        return mainWrapperList;
    }

    @AuraEnabled
    public static List<Field_Mapping_Setting_Child__mdt> getFieldMappingSettingChildMdt(){
        try{
            List<Field_Mapping_Setting_Child__mdt> fieldMappingStgChildMdtList = new List<Field_Mapping_Setting_Child__mdt>();
            fieldMappingStgChildMdtList = [SELECT Id, MasterLabel, DeveloperName, NamespacePrefix,  Field_Api_Name__c, Field_Label__c, Field_Mapping_Setting__c, Field_Type__c, Field_Value__c, Is_Deleted1__c FROM Field_Mapping_Setting_Child__mdt  ];
            system.debug('query field mapping mdt= '+fieldMappingStgChildMdtList);
          return fieldMappingStgChildMdtList;      
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    @AuraEnabled
    public static List<Field_Mapping_Setting__mdt> getFieldMappingSettingMdt(){
        try{
            List<Field_Mapping_Setting__mdt> fieldMappingStgMdtList = new List<Field_Mapping_Setting__mdt>();
            fieldMappingStgMdtList = [SELECT Id, MasterLabel, DeveloperName, NamespacePrefix,  Object_Api_Name__c, Object_Label__c, Action_Name__c, Action_Type__c FROM Field_Mapping_Setting__mdt  ];
            system.debug('query field mapping mdt= '+fieldMappingStgMdtList);
          return fieldMappingStgMdtList;      
        }catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static list<SobjectListWrapper> getObjectList(){
        try {
            list<SobjectListWrapper> allSobjectList = new list<SobjectListWrapper>();
                Map<string, schema.sObjectType> sObjectMap = Schema.getGlobalDescribe(); 
                System.debug('object Results ' + sObjectMap);
                
                Set<String> allOjbectNames =  sObjectMap.keySet();
                list<Schema.SObjectType> allOjbectValue =  sObjectMap.values();
                list<string> sObjectList = new list<string>();
              
                for(SObjectType valueObj: allOjbectValue)
                {
                    sObjectList.add(String.valueOf(valueObj));
                    
                }
                List<Schema.DescribeSObjectResult> describeSobjectsResult = Schema.describeSObjects(sObjectList); 
               // describeSobjectsResult.sort();
               System.debug('describe Sobjects Result ' + describeSobjectsResult);
                for(Schema.DescribeSObjectResult ob: describeSobjectsResult)
                {
                    
                        SobjectListWrapper wrapObj = new SobjectListWrapper();
                        wrapObj.label = ob.getLabel();
                        wrapObj.value = ob.getName();
                        
                        allSobjectList.add(wrapObj);      
                }
                allSobjectList.sort();
                
            system.debug('wrap list = '+allSobjectList);
            return allSobjectList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }


    public class SobjectListWrapper implements Comparable{
        @AuraEnabled
        public string value;
        @AuraEnabled
        public string label; 
        
       

        public Integer compareTo(Object compareTo) {
            // needs bus logic to decide if null date is < non null date or vice-versa
            SobjectListWrapper cIW = (SobjectListWrapper) compareTo;
            if (cIW.label < this.label)
              return +1;
            if (cIW.label > this.label)
              return -1;
            return 0;
          }
    }

    public class FieldsListMainWrapper{
       
        @AuraEnabled
        public List<Field_Mapping_Setting_Child__mdt> fieldMappingStgChildList ;

        @AuraEnabled
        public List<Field_Mapping_Setting__mdt> fieldMappingStgList ;

        @AuraEnabled
        public list<SobjectListWrapper> sObjectList;

    }
    public class SobjectFieldsListWrapper{
       
        @AuraEnabled
        public string label;
        @AuraEnabled
        public string value; 
        @AuraEnabled
        public string type;
        @AuraEnabled
        public Boolean isReqField;
        @AuraEnabled
        public string fieldValue;
        @AuraEnabled
        public string Id;
        
    }
}
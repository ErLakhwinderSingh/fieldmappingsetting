import { LightningElement, track, api } from 'lwc';
import getPickListValuesIntoList from "@salesforce/apex/FieldMappingController.getPickListValuesIntoList";
import {
    javaScriptError,
    setObjectRecord,
    keyMaker,
    apexCaller,
    showToast,
    showErrorToast,
    sortList
} from "c/dataTableBase";
export default class GetFieldPickListValueChild extends LightningElement {

   
    @api fieldType;   
    @api isReqField;
    @api fieldApiName;
    @api fieldValue;
    isShowSpinner=false;
    @api sObjectApiName;
    showPickList =false;
    @track pickListValueOptions;

    connectedCallback(){
        try {
            console.log('child get picklist value child call');
                       
            if (this.isReqField== true && this.fieldType === 'PICKLIST') {
                this.showPickList = true;
                console.log('this.sObjectApiName picklist= ', this.sObjectApiName);
                
                console.log('this.fieldApiName  picklist=', this.fieldApiName);

                this.getPickListValue(this.sObjectApiName, this.fieldApiName);
                       
            }
           

        } catch (err) {
            javaScriptError(err, self);
        }
    }

    getPickListValue(objectApiName,fieldApiName) {
        try {
            const self = this;
            this.isShowSpinner = true;
            const params = { sObjectApiName: objectApiName, fieldApiName: fieldApiName };
            apexCaller(getPickListValuesIntoList, params, self, function (response) {
                this.isShowSpinner = false;
                try {
                    if (response) {
                        this.pickListValueOptions = response; 
                        console.log('all picklist this.pickListValueOptions= ', this.pickListValueOptions);
                    }
                    
                } catch (err) {
                    javaScriptError(err, self);
                }
            });
           
            
        } catch (err) {
            javaScriptError(err, self);
        }
    }

    fieldInputValueHandler(event){
        let value =event.target.value;
        console.log('intput', value);
        this.dispatchFieldInputAction(value);
        
    }

    dispatchFieldInputAction(dispatchInput)
    {
        console.log('dispatchInput', dispatchInput);
        const sendNewChildRecord = new CustomEvent('sendinput', {detail:{value:dispatchInput}});

        this.dispatchEvent(sendNewChildRecord);
    }
}
import { LightningElement, track, api } from 'lwc';
import insertFieldMappingSettingAndChildMdt from "@salesforce/apex/FieldMappingController.insertFieldMappingSettingAndChildMdt";
import getFieldMappingQueryLists from "@salesforce/apex/FieldMappingController.getFieldMappingQueryLists";
import getFieldList from "@salesforce/apex/FieldMappingController.getFieldList";
import getPickListValuesIntoList from "@salesforce/apex/FieldMappingController.getPickListValuesIntoList";

import { NavigationMixin } from "lightning/navigation";

import {
    javaScriptError,
    setObjectRecord,
    keyMaker,
    apexCaller,
    showToast,
    showErrorToast,
    sortList
} from "c/dataTableBase";

export default class FieldMappingSetting extends NavigationMixin(LightningElement) {

    isShowSpinner = false;
    @track fieldMappingStgRecordList;
    @track fieldMappingStgExistRecordList;
    @track fieldMappingStgChildRecordList;
    @track delExistRecordChildList = [];
    @track newFieldMappingStgChildRecList = [];
    @track sOjectList;

    @track rowAddFlag;
    keyIndex = 0;
    rowList = [];

    @track allNewFieldList = [];
    @track mainFieldList = [];
    @track fieldFilterList;
    
    
    actionName;
    actionType;
    @track parentDeveloperName;
    sObjectName;
    // show Exist 
    preDefValueList = [];
    addPredefineValue = false;


    filterListFlag = false;
    clearAndUpdateFlag = false;
    updateNewFieldListFlag =false;
    duringUpdateChangeSObjectFlag=false;
    addNewRowFlag = false;
    actionNameId;
    
    
    
    
    get actionTypeOptions() {
        return [
            { label: 'Create a Record', value: 'Create a Record' },
            { label: 'Update Records', value: 'Update Records' },
        ];
    }

    connectedCallback() {
        try {
            this.getQueryRecordAction();

        } catch (err) {
            javaScriptError(err, self);
        }
    }

    getQueryRecordAction() {
        try {
            const self = this;
            const params = {};
            apexCaller(getFieldMappingQueryLists, params, self, function (response) {
                //this.isShowSpinner = false;
                try {
                    if (response) {
                        let arr = [];
                        this.fieldMappingStgRecordList = response.fieldMappingStgList;
                        this.fieldMappingStgChildRecordList = response.fieldMappingStgChildList;
                        this.sOjectList = response.sObjectList;

                        this.fieldMappingStgExistRecordList = this.setFieldMappingListForComboBoxAction(this.fieldMappingStgRecordList); 
                        console.log('this.fieldMappingStgRecordList',this.fieldMappingStgRecordList);  
                    }
                } catch (err) {
                    javaScriptError(err, self);
                }
            });

        } catch (err) {
            javaScriptError(err, self);
        }
    }

    setFieldMappingListForComboBoxAction(fieldMappingExistRecordList) {
        try {
            let tempArray = [];
            fieldMappingExistRecordList.forEach(cur => {
                tempArray.push({
                    Id: cur.Id,
                    label: cur.Action_Name__c,
                    value: cur.Id,
                    DeveloperName: cur.DeveloperName,
                    Object_Api_Name__c: cur.Object_Api_Name__c
                });
            });
            console.log('Existing Record list for combobox=', tempArray);
            return tempArray;

        } catch (err) {
            javaScriptError(err, self);
        }
    }

    selectActionNameHandler(event) {
        try {
            this.actionNameId = event.detail.value;

            this.getAndSetExistParentPropertyAction(this.actionNameId);

            this.addPredefineValue = true;
            this.mainFieldList = [];

            this.preDefValueList = this.getAndSetExistChildRecAction(this.actionNameId);
            console.log('this.preDefValueList', this.preDefValueList);

            this.getAllFieldList(this.sObjectName);

            this.clearAndUpdateFlag = true;
            this.updateNewFieldListFlag= true;
            this.addNewRowFlag = true;

        } catch (err) {
            javaScriptError(err, self);
        }
    }
    getAndSetExistParentPropertyAction(selectParentRecId) {
        try {
            this.fieldMappingStgRecordList.forEach(cur => {
                if (selectParentRecId == cur.Id) {
                    this.sObjectName = cur.Object_Api_Name__c;
                    this.parentDeveloperName = cur.DeveloperName;
                    this.actionType = cur.Action_Type__c;
                    this.actionName = cur.Action_Name__c;
                    
                }
            });
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }
    getAndSetExistChildRecAction(selectParentRecId) {

        try {
            let tempArray = [];
            this.fieldMappingStgChildRecordList.forEach(cur => {
                if (cur.Field_Mapping_Setting__c === selectParentRecId) {
                    tempArray.push({
                        isReqField: true,
                        Id: cur.Id,
                        label: cur.Field_Label__c,
                        type: cur.Field_Type__c,
                        value: cur.Field_Api_Name__c,
                        fieldValue: cur.Field_Value__c,
                        isDeleted: cur.Is_Deleted1__c,
                        DeveloperName: cur.DeveloperName
                    });
                }
            });
            return tempArray;
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }

    actionNameInputHandler(event) {
        try {
            if(this.actionName == null && this.actionName == undefined){
                this.sObjectName =''; 
            }
               
            this.actionName = event.detail.value;
            console.log(' actionName=', this.actionName);
            if (this.clearAndUpdateFlag == true) {
                this.updateParentListAction(this.fieldMappingStgRecordList, 'Action_Name__c', this.actionName);
                console.log(' udpate parent Action Name =', this.fieldMappingStgRecordList);
            }
        } catch (err) {
            javaScriptError(err, self);
        }
    }

    selectActionTypeHandler(event) {
        try {
            this.actionType = event.detail.value;
            console.log('actionType', this.actionType);
            if (this.clearAndUpdateFlag == true) {
                this.updateParentListAction(this.fieldMappingStgRecordList, 'Action_Type__c', this.actionType);
                console.log(' update Parent Action type  =', this.fieldMappingStgRecordList);
            }
        } catch (err) {
            javaScriptError(err, self);
        }

    }

    
    selectSobjectHandleAction(event) {
        try {
            //let actionNameEmpty = 
            console.log('this actionname = ', this.actionName);
            if(this.actionName == null && this.actionName == undefined)
            {
                
                this.fieldValidationChecker('lightning-input');
                //this.template.querySelector('input').classList.add('slds-has-error');
                return ;
                
            }
            
            this.sObjectName = event.detail.value;
            this.duringUpdateChangeSObjectFlag=true;
            this.updateNewFieldListFlag= false;
            this.filterListFlag = false;
            this.addNewRowFlag = true;
            console.log('s object ', this.sObjectName);
            if (this.clearAndUpdateFlag == true) {
                this.setDelFieldMappingChildRecordAction();
                this.newFieldMappingStgChildRecList = [];
                this.preDefValueList = [];
            }
            this.getAllFieldList(this.sObjectName);
            this.setSobjectListAction(this.sObjectName);
            this.rowList = [];
        } catch (err) {
            javaScriptError(err, self);
        }

    }
    fieldValidationChecker(lightning_type) {
        return [...this.template.querySelectorAll(lightning_type)].reduce(
          (validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
          },
          true
        );
      }
    setDelFieldMappingChildRecordAction() {

        this.preDefValueList.forEach(cur => {
            this.delExistRecordChildList.push(cur.DeveloperName);
        });
    }

    setSobjectListAction(sObjectName) {
        try {
            let tempObj;
            let sObjUpdateLabel;
            if (this.clearAndUpdateFlag == false) {
                this.sOjectList.forEach(cur => {
                    if (cur.value == sObjectName) {
                       
                            this.parentDeveloperName = this.actionName.trim().replace(/ +/g, "");
                            console.log(' parentDeveloperName =', this.parentDeveloperName);
                       
                        tempObj = {
                            Id: null,
                            DeveloperName: this.actionName.trim().replace(/ +/g, ""),
                            Object_Api_Name__c: cur.value,
                            Object_Label__c: cur.label,
                            Action_Name__c: this.actionName,
                            Action_Type__c: this.actionType
                        };
                    }
                });
            this.fieldMappingStgRecordList.push(tempObj);
            console.log(' after final list object =', this.fieldMappingStgRecordList);
            }


            if (this.clearAndUpdateFlag == true) {
                sObjUpdateLabel = this.getAndSetValueAction(this.sOjectList, 'value', sObjectName, 'label');

                this.updateParentListAction(this.fieldMappingStgRecordList, 'Object_Api_Name__c', sObjectName);
                this.updateParentListAction(this.fieldMappingStgRecordList, 'Object_Label__c', sObjUpdateLabel);

                console.log(' fieldMappingStgRecordList updated  =', this.fieldMappingStgRecordList);
            }

        }
        catch (err) {
            javaScriptError(err, self);
        }

    }

    getAndSetValueAction(listParams, value, compareValue, getProperty) {
        try {
            let result;
            listParams.forEach(cur => {
                if (cur[value] == compareValue) {
                    result = cur[getProperty];
                }
            });
            return result;
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }

    updateParentListAction(listForUpdate, fieldName, updatedValue) {
        try {
            listForUpdate.forEach(cur => {
                if (cur.Id == this.actionNameId) {
                    cur[fieldName] = updatedValue;
                }
            });
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }
    // opt
    getAllFieldList(sObjectName) {
        try {
            const self = this;
            this.isShowSpinner = true;
            const params = { sObjectName: sObjectName };
            apexCaller(getFieldList, params, self, function (response) {
                this.isShowSpinner = false;
                try {
                    if (response) {
                        this.allFieldList = response;
                        //exist record list
                        if (this.addPredefineValue == true) {
                            this.addPredefineValue = false;
                            this.setExistFieldMappingChildPreDefineAction();
                            console.log('exist record list ', this.mainFieldList);
                        }
                        //New field record list
                        else if (this.rowAddFlag == true) {
                            this.mainFieldList = this.allFieldList.filter(cur => cur.isReqField == true);
                            console.log(' first time mainFieldList= ', this.mainFieldList);
                        }
                        else {
                            this.mainFieldList = response;
                            console.log('main field list ', this.mainFieldList);
                        }
                    }

                } catch (err) {
                    javaScriptError(err, self);
                }
            });
        } catch (err) {
            javaScriptError(err, self);
        }
    }
    setExistFieldMappingChildPreDefineAction(){
        this.preDefValueList.forEach(cur => {
            this.mainFieldList.push(cur);
        });

    }

    receivedFieldFilterList(event) {
        try {
            this.fieldFilterList = event.detail;
            console.log('this.fieldFilterList ', this.fieldFilterList);
            this.filterListFlag = true;
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }


    addRow() {
        try {
            if (this.addNewRowFlag == true) {
                this.rowAddFlag = true;
                this.keyIndex++;

                var newRow = [{ Id: this.keyIndex }];
                this.rowList = this.rowList.concat(newRow);

                console.log('this.updateNewFieldListFlag',this.updateNewFieldListFlag);
                console.log('this.this.filterListFlag',this.filterListFlag);

                if (this.updateNewFieldListFlag == true) {
                    if (this.duringUpdateChangeSObjectFlag == true) {
                        this.allNewFieldList = this.allFieldList.filter(cur => cur.isReqField == false);
                        console.log(' update flag List  =', this.allNewFieldList);
                    }
                    else if (this.filterListFlag == true) {
                        this.allNewFieldList = this.fieldFilterList;
                        console.log('update > filter field List  =', this.allNewFieldList);
                    }
                    
                    else {
                        this.allNewFieldList = this.setAddNewFieldListAction(this.allFieldList, this.preDefValueList);
                        console.log(' after update List  =', this.allNewFieldList);
                    }
                }
                else if (this.filterListFlag == true) {
                    this.allNewFieldList = this.fieldFilterList;
                    console.log('filter field List  =', this.allNewFieldList);
                }
                else {
                    this.allNewFieldList = this.allFieldList.filter(cur => cur.isReqField == false);
                    console.log(' frest  List  =', this.allNewFieldList);
                }

                console.log('this.rowList', this.rowList);
                this.mainFieldList = [...this.mainFieldList, ...this.rowList];
                console.log('this.mainFieldList', this.mainFieldList);
            }

        } catch (err) {
            javaScriptError(err, self);
        }
    }
    setAddNewFieldListAction(allFieldList, selectFieldList) {
        try {
            let tempArray = [];
            selectFieldList.forEach(cur => {
                tempArray.push(cur.value);
            });

            return allFieldList = allFieldList.filter(item => !tempArray.includes(item.value));

        } catch (err) {
            javaScriptError(err, self);
        }
    }

    delExistRecordHandler(event) {
        try {
            let accessKey = event.detail.delAccessKey;
            console.log(' this.accessKey', accessKey);
            console.log(' this.mainFieldList', this.mainFieldList);
            this.mainFieldList.forEach(cur => {
                if (cur.Id == accessKey) {
                    this.delExistRecordChildList.push(cur.DeveloperName);
                }
            });

            this.mainFieldList = this.mainFieldList.filter(function (element) {
                return element.Id !== accessKey;
            });
            console.log(' this.delExistRecordChildList', this.delExistRecordChildList);

        } catch (err) {
            javaScriptError(err, self);
        }
    }

    delNewFieldRowHandler(event) {
        try {
            let accessKey = event.detail.delAccessKey;
            console.log(' this.accessKey', accessKey);
            let delListItem = event.detail.delList;

            this.removeRow(accessKey);
             this.resetFieldFilterListAction(delListItem);

            this.newFieldMappingStgChildRecList = this.newFieldMappingStgChildRecList.filter(cur => cur.Id !== accessKey);
            console.log(' this.newFieldMappingStgChildRecList', this.newFieldMappingStgChildRecList);
        } catch (err) {
            javaScriptError(err, self);
        }
    }
    removeRow(accessKey) {

        try {
            if (this.rowList.length > 0) {
                this.rowList = this.rowList.filter(function (element) {
                    return parseInt(element.Id) !== parseInt(accessKey);
                });
                // del issue here
               // this.mainFieldList = this.mainFieldList.filter(cur => accessKey != cur.Id);
            }
            console.log('this.mainFieldList= ', this.mainFieldList);
        } catch (err) {
            javaScriptError(err, self);
        }
    }
    resetFieldFilterListAction(delList) {
        try {
            delList.forEach(cur => {
                this.fieldFilterList.push(cur);
            });
            console.log('this.fieldFilterList =', this.fieldFilterList);

        } catch (err) {
            javaScriptError(err, self);
        }
    }

    
    receivedNewChildRecord(event) {

        try {
            let newChildRecord = event.detail;
            this.newFieldMappingStgChildRecList.push(newChildRecord);
            this.newFieldMappingStgChildRecList = this.removeDuplicates(this.newFieldMappingStgChildRecList, 'Field_Api_Name__c');

            console.log('newFieldMappingStgChildRecList   = ', this.newFieldMappingStgChildRecList);

        } catch (err) {
            javaScriptError(err, self);
        }
    }
    removeDuplicates(arrList, compareProperty) {
        try {
            let newArray = [];
            let uniqueObject = {};
            for (let i in arrList) {
                let objTitle = arrList[i][compareProperty];
                // Use the title as the index
                uniqueObject[objTitle] = arrList[i];
            }
            for (let i in uniqueObject) {
                newArray.push(uniqueObject[i]);
            }
            console.log(newArray);
            return newArray;

        } catch (err) {
            javaScriptError(err, self);
        }
    }


    recordSaveActionHandler() {
        try {

            console.log('null list = ', this.parentDeveloperName, this.fieldMappingStgRecordList, this.newFieldMappingStgChildRecList, this.delExistRecordChildList);
            if (
                this.parentDeveloperName != null &&
                this.fieldMappingStgRecordList != null &&
                this.newFieldMappingStgChildRecList != null &&
                this.delExistRecordChildList != null
            ) {
                const self = this;

                this.isShowSpinner = true;


                console.log('final list object =', this.fieldMappingStgRecordList);
                console.log('final list  field =', this.newFieldMappingStgChildRecList);
                console.log('final list del =', this.delExistRecordChildList);

                const params = {
                    parentDeveloperName: this.parentDeveloperName,
                    fieldMappingStgRecordList: this.fieldMappingStgRecordList,
                    fieldMappingSettingChildList: this.newFieldMappingStgChildRecList,
                    deleteFieldMappingChildRecord: this.delExistRecordChildList

                };
                apexCaller(insertFieldMappingSettingAndChildMdt, params, self, function (response) {

                    try {
                        console.log('responce = ', response);
                        if (response == null) {
                            console.log('delay call = ');
                            this.delayCall();
                        }
                        else {
                            // console.log('responce = ', response);                   
                            this.isShowSpinner = false;
                            showToast(
                                "Success!",
                                "success",
                                'Record Save Successfully Wait 10 Second And Refesh Again',
                                null,
                                3000,
                                self
                            );
                            this.sObjectName = '';

                            this.refeshCall();
                            console.log('response save record= ', response);
                        }
                    } catch (err) {
                        javaScriptError(err, self);
                    }
                });
            }
            else {
                console.log('else save function check null');
            }
        } catch (err) {
            javaScriptError(err, self);
        }
    }

    delayCall() {

        try {
            setTimeout(() => { this.recordSaveActionHandler() }, 1000);
        } catch (err) {
            javaScriptError(err, self);
        }
    }
    refeshCall() {

        try {
            setTimeout(() => { this.resetPageAction() }, 3000);
        } catch (err) {
            javaScriptError(err, self);
        }
    }
    resetPageAction() {

        try {
            const config = {
                type: 'standard__webPage',
                attributes: {
                    url: 'https://stadium1--uat.lightning.force.com/lightning/page/home'
                }
            };
            this[NavigationMixin.Navigate](config);
        } catch (err) {
            javaScriptError(err, self);
        }
    }

}
import { LightningElement, track, api } from 'lwc';
import getPickListValuesIntoList from "@salesforce/apex/FieldMappingController.getPickListValuesIntoList";
import {
    javaScriptError,
    setObjectRecord,
    keyMaker,
    apexCaller,
    showToast,
    showErrorToast,
    sortList
} from "c/dataTableBase";

export default class FieldMappingSettingChildOne extends LightningElement {

    @api fieldLabel;
    @api fieldType;
    @api isReqField;
    @api fieldApiName;
    @api fieldValue;
    @api isDeleted;
    
    @api parentDeveloperName;
    @api sObjectApiName;

    @api rowId;
    @api rowAddFlag;
    @api addNewFieldList;
    
    @track tempReqAndNewFieldList;
    @track selectedNewfield = [];

    showCustomDropdown = false;
    showPickList = false;

    dropdownSelectOptions = '';
    pickListValueOptions;
   
    get fieldTypeOptions() {
        return [
            { label: this.fieldType, value: this.fieldType },
            { label: "Field Reference", value: "Field Reference" },
            { label: "Formula", value: "Formula" }
        ];
    }


    customListOnClickHandler(event) {
        this.showCustomDropdown = true;
    }
    dropDownCloseHandler(event) {
        this.showCustomDropdown = false;
    }
    // handleBlur(event){
    //     this.dropDownCloseHandler();
    // }

    removeRow(event) {
        try {
            let tempArray = [];
            let delAccessKey = event.target.accessKey;
            tempArray = this.setDelFieldListAction(delAccessKey);

            let sendDelDetail = { delAccessKey: delAccessKey, delList: tempArray };
            const sendDelAccessKey = new CustomEvent('accesskey', { detail: sendDelDetail });
            this.dispatchEvent(sendDelAccessKey);
        } catch (err) {
            javaScriptError(err, self);
        }
    }

    setDelFieldListAction(delAccessKey) {

        try {
            let tempArray = [];
            this.selectedNewfield.forEach(cur => {
                if (delAccessKey == cur.Id) {
                    tempArray.push(cur);
                }
            });
            return tempArray;
        } catch (err) {
            javaScriptError(err, self);
        }
    }

    selectNewFieldAction(event) {
        try {

            this.dropDownCloseHandler();
            let dataset = event.currentTarget.dataset;

            this.fieldApiName = dataset.value;
            this.fieldLabel = dataset.label;
            
            this.dropdownSelectOptions = this.fieldLabel;

            this.setSelectFieldListAction(this.fieldApiName);
            this.fieldFilterListAction(this.fieldApiName);


        } catch (err) {
            javaScriptError(err, self);
        }
    }

    setSelectFieldListAction(value) {

        try {
            this.addNewFieldList.forEach(cur => {
                if (value == cur.value) {
                    this.fieldType = cur.type;
                    this.rqValue = cur.isReqField;
                    this.selectedNewfield.push({ label: cur.label, value: cur.value, type: cur.type, Id: this.rowId });
                }

            });
            if (this.fieldType === 'PICKLIST') {
                this.showPickList = true;
                this.getPickListValue(this.sObjectApiName, this.fieldApiName);
                console.log('this.pickListValueOptions ', this.pickListValueOptions);
            }
            else {
                this.showPickList = false;
                this.clear();
            }
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }

    fieldFilterListAction(value) {

        try {
            let tempFilterList = this.addNewFieldList.filter(cur => value != cur.value);
            console.log(' tempFilterList ', tempFilterList);
            const sendFilterList = new CustomEvent('filterlist', { detail: tempFilterList });
            this.dispatchEvent(sendFilterList);
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }

    fieldTypeChangeAction(event) {
        try {
            this.changeFieldType = event.detail.value;
            console.log(' this.changeFieldType ', this.changeFieldType);
        } catch (err) {
            javaScriptError(err, self);
        }
    }

    getPickListValue(objectApiName, fieldApiName) {
        try {
            const self = this;
            this.isShowSpinner = true;
            const params = { sObjectApiName: objectApiName, fieldApiName: fieldApiName };
            apexCaller(getPickListValuesIntoList, params, self, function (response) {
                this.isShowSpinner = false;
                try {
                    if (response) {
                        this.pickListValueOptions = response;
                        console.log('all picklist this.pickListValueOptions= ', this.pickListValueOptions);
                    }

                } catch (err) {
                    javaScriptError(err, self);
                }
            });


        } catch (err) {
            javaScriptError(err, self);
        }
    }

    clear() {

        try {
            this.fieldValue = null;
            console.log('clear');
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }

    fieldInputValueHandler(event) {

        try {
            let inputValue = event.detail.value;
            console.log('inpute value = ', inputValue);
            this.setFieldMappingStgChildListAction(inputValue);
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }

    setFieldMappingStgChildListAction(value) {

        try {
            let isDeleted;
            this.rqValue == false ? isDeleted = 'true' : isDeleted = 'false';

            let developerName = `${this.parentDeveloperName}${this.sObjectApiName}${this.fieldApiName}`;
            console.log(' developerName =', developerName);

            if (this.changeFieldType != null && this.changeFieldType != '') {
                this.tempReqAndNewFieldList = { Id: this.rowId, DeveloperName: developerName, Field_Label__c: this.fieldLabel, Field_Api_Name__c: this.fieldApiName, Field_Type__c: this.changeFieldType, Field_Value__c: value, Is_Deleted1__c: isDeleted };
            }
            else {
                this.tempReqAndNewFieldList = { Id: this.rowId, DeveloperName: developerName, Field_Label__c: this.fieldLabel, Field_Api_Name__c: this.fieldApiName, Field_Type__c: this.fieldType, Field_Value__c: value, Is_Deleted1__c: isDeleted };
            }
            console.log('this.tempReqAndNewFieldList=', this.tempReqAndNewFieldList);

            this.dispatchchildRecAction(this.tempReqAndNewFieldList);
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }

    dispatchchildRecAction(dispatchList) {

        try {
            const sendNewChildRecord = new CustomEvent('newchildrecord', { detail: dispatchList });
            this.dispatchEvent(sendNewChildRecord);
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }

    resetDeveloperNameAction(uniqueToken, value) {

        try {
            //let developerName = label.trim().replace(/ +/g, "");
            //var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

            //if (format.test(value)) {
            return `${uniqueToken}${value}`;
            // } else {
            //     return label.trim().replace(/ +/g, "");
            // }
        }
        catch (err) {
            javaScriptError(err, self);
        }
    }



}